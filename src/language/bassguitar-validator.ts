import type {  ValidationChecks } from 'langium';
import type { BassguitarAstType } from './generated/ast.js';
import type { BassguitarServices } from './bassguitar-module.js';

/**
 * Register custom validation checks.
 */
export function registerValidationChecks(services: BassguitarServices) {
    const registry = services.validation.ValidationRegistry;
    const validator = services.validation.BassguitarValidator;
    const checks: ValidationChecks<BassguitarAstType> = {
        
    };
    registry.register(checks, validator);
}

/**
 * Implementation of custom validations.
 */
export class BassguitarValidator {

    

}
