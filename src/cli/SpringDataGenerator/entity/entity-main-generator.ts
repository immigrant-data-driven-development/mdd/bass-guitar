import fs from "fs";
import { Application } from "../../../language/generated/ast.js";
import { generateConfigs } from "./config-generator.js";
import { generateModules } from "./module-generator.js";
import {generateSchemaSQLHelper} from "./sql-generator.js";
import {generateDebezium} from "./debezium-generator.js"

export function generateEntiySpringData(model: Application, target_folder: string) : void {
  fs.mkdirSync(target_folder, {recursive:true})
  generateConfigs(model, target_folder);
  generateModules(model, target_folder);
  generateSchemaSQLHelper(model,target_folder);
  generateDebezium(model,target_folder)


}