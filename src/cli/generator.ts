import type { Application } from '../language/generated/ast.js';

import * as path from 'node:path';
import { generateMainSpringData } from "./SpringDataGenerator/main-generator.js";
import { generateDocumentation } from "./DocumentationGenerator/main-generator.js";

export function generateJavaScript(model: Application, filePath: string, destination: string | undefined): string {
    const final_destination = extractDestination(filePath, destination)
    
    generateMainSpringData(model, final_destination)
    generateDocumentation(model,final_destination)
    
    return final_destination;
}

function extractDestination(filePath: string, destination?: string) : string {
    const path_ext = new RegExp(path.extname(filePath)+'$', 'g')
    filePath = filePath.replace(path_ext, '')
  
    return destination ?? path.join(path.dirname(filePath), "generated")
  }